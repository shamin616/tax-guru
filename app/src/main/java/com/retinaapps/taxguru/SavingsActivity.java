package com.retinaapps.taxguru;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.retinaapps.taxguru.Data.SavingsTypes;
import com.retinaapps.taxguru.Database.AppDatabase;
import com.retinaapps.taxguru.Database.Savings;

public class SavingsActivity extends AppCompatActivity {

    TextView SavingsAddTypeTextView;
    Button SavingsAddButton;
    EditText SavingsAddAmount;
    SetSavings setSavings;
    SavingsTypes.SavingsType savingsType;
    boolean isUpdate = false;
    int savingsId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_savings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        SavingsAddTypeTextView = findViewById(R.id.savings_add_type_text);
        SavingsAddButton = findViewById(R.id.savings_add_submit);
        SavingsAddAmount = findViewById(R.id.savings_add_amount);
        int savingsTypeId = intent.getIntExtra(SavingsChooseActivity.SAVINGS_TYPE_ID,-1);
        if(savingsTypeId == -1)
        {
            savingsId = intent.getIntExtra(HomeActivityFragment.SAVINGS_ID,-1);
            Log.e("Savings Type",""+ savingsId);
            if(savingsId != -1)
            {
                isUpdate = true;
                GetSavings getSavings = new GetSavings();
                getSavings.execute(savingsId);
            }
        }
        else
        {
            setSavingsTypeText(savingsTypeId);
        }

        SavingsAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSavings = new SetSavings();
                setSavings.execute();
            }
        });
    }
    public void setSavingsTypeText(int savingsTypeId)
    {
        SavingsTypes savingsTypes = new SavingsTypes();
        savingsType = savingsTypes.getSavingsType(savingsTypeId);
        SavingsAddTypeTextView.setText(savingsType.getmType());
    }
    private class SetSavings extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                    AppDatabase.class, "com.retinaapps.taxguru").build();
            if(isUpdate)
            {
                Savings savings = db.savingsDao().findById(savingsId);
                savings.setAmount(Float.parseFloat(SavingsAddAmount.getText().toString()));
                db.savingsDao().update(savings);
            }
            else
            {
                Savings savings = new Savings(savingsType.getmId(), Float.parseFloat(SavingsAddAmount.getText().toString()));
                db.savingsDao().insert(savings);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            finish();
        }

    }

    private class GetSavings extends AsyncTask<Integer, Void, Savings> {
        @Override
        protected Savings doInBackground(Integer... params) {
            AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                    AppDatabase.class, "com.retinaapps.taxguru").build();
            return db.savingsDao().findById(params[0]);
        }

        @Override
        protected void onPostExecute(Savings savings) {
            super.onPostExecute(savings);
            if(savings != null)
            {
                setSavingsTypeText(savings.getType());
                SavingsAddAmount.setText(Float.toString(savings.getAmount()));
            }
        }
    }


}
