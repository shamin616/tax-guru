package com.retinaapps.taxguru;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.retinaapps.taxguru.Data.SavingsTypes;

public class SavingsChooseActivity extends AppCompatActivity implements SavingsTypeListFragment.OnListFragmentInteractionListener{

    public static final String SAVINGS_TYPE_ID = "savings_type_id";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_savings_choose);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onListFragmentInteraction(SavingsTypes.SavingsType item) {
        Intent intent = new Intent(this,SavingsActivity.class);
        intent.putExtra(SAVINGS_TYPE_ID,item.getmId());
        startActivity(intent);
    }
}
