package com.retinaapps.taxguru;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.retinaapps.taxguru.Database.AppDatabase;
import com.retinaapps.taxguru.Database.Income;

import java.util.List;

public class AddIncomeActivity extends AppCompatActivity {

    EditText IncomeEditText;
    Button IncomeSubmit;
    Button MonthlyIncome;
    Float totalIncome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_income);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        IncomeEditText = findViewById(R.id.income_text_edit);
        IncomeSubmit = findViewById(R.id.income_submit);
        MonthlyIncome = findViewById(R.id.monthly_income_button);
        GetIncome getIncome = new GetIncome();
        getIncome.execute();
        IncomeSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Float income = Float.valueOf(IncomeEditText.getText().toString());
                AddIncome addIncome = new AddIncome();
                addIncome.execute(income);
                finish();
            }
        });

        MonthlyIncome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AddIncomeMonthly.class);
                startActivity(intent);
            }
        });
    }
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    private class AddIncome extends AsyncTask<Float, Void, Void> {
        @Override
        protected Void doInBackground(Float... params) {
            AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                    AppDatabase.class, "com.retinaapps.taxguru").build();
            Income income = new Income(1, params[0] / 12, params[0] / 12, params[0] / 12, params[0] / 12, params[0] / 12, params[0] / 12, params[0] / 12, params[0] / 12, params[0] / 12, params[0] / 12, params[0] / 12, params[0] / 12, params[0]);
            if (totalIncome == 0f) {
                db.incomeDao().insert(income);
            }
            {
                db.incomeDao().update(income);
            }
            return null;
        }
    }

    private class GetIncome extends AsyncTask<Void, Void, Float> {
        @Override
        protected Float doInBackground(Void... params) {
            AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                    AppDatabase.class, "com.retinaapps.taxguru").build();
            List<Income> income = db.incomeDao().getAll();
            if (income.size() > 0) {
                return income.get(0).getTotalAmount();
            } else {
                return 0f;
            }
        }

        @Override
        protected void onPostExecute(Float aFloat) {
            super.onPostExecute(aFloat);
            IncomeEditText.setText(Float.toString(aFloat));
            totalIncome = aFloat;
        }
    }
}
