package com.retinaapps.taxguru;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.retinaapps.taxguru.Data.SavingsTypes;
import com.retinaapps.taxguru.SavingsTypeListFragment.OnListFragmentInteractionListener;

import java.util.ArrayList;
import java.util.List;

public class MySavingsTypeRecyclerViewAdapter extends RecyclerView.Adapter<MySavingsTypeRecyclerViewAdapter.ViewHolder> {

    private final ArrayList<SavingsTypes.SavingsType> mValues;
    private final OnListFragmentInteractionListener mListener;
    private Context context;

    public MySavingsTypeRecyclerViewAdapter(ArrayList<SavingsTypes.SavingsType> items, OnListFragmentInteractionListener listener,Context context) {
        mValues = items;
        mListener = listener;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_savingstype, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mContentView.setText(mValues.get(position).getmType());
        holder.mIconView.setImageResource(mValues.get(position).getmIcon());
        holder.mIconView.setColorFilter(ContextCompat.getColor(context, R.color.color_icon_pink), android.graphics.PorterDuff.Mode.SRC_IN);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mContentView;
        public final ImageView mIconView;
        public SavingsTypes.SavingsType mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mContentView = (TextView) view.findViewById(R.id.savings_type);
            mIconView = view.findViewById(R.id.savings_type_icon);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
