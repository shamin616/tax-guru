package com.retinaapps.taxguru.Database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by apple on 27/01/18.
 */
@Entity(tableName = "savings")
public class Savings {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "type")
    private int mType;

    @ColumnInfo(name = "amount")
    private float mAmount;

    public Savings(int type, float amount)
    {
        this.mType = type;
        this.mAmount = amount;
    }

    public int getId()
    {
        return id;
    }

    public int getType()
    {
        return mType;
    }

    public float getAmount()
    {
        return mAmount;
    }

    public void setAmount(float mAmount) {
        this.mAmount = mAmount;
    }

    public void setId(int id) {
        this.id = id;
    }
}
