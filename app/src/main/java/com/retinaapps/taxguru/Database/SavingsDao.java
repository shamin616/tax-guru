package com.retinaapps.taxguru.Database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Created by apple on 27/01/18.
 */
@Dao
public interface SavingsDao {
    @Query("SELECT * FROM savings")
    List<Savings> getAll();

    @Query("SELECT * FROM savings WHERE id = :id ")
    Savings findById(int id);

    @Insert
    void insert(Savings savings);

    @Delete
    void delete(Savings savings);

    @Update
    void update(Savings savings);
}
