package com.retinaapps.taxguru.Database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by apple on 27/01/18.
 */
@Entity(tableName = "income")
public class Income {
    @PrimaryKey
    @ColumnInfo(name = "id")
    int mId;

    @ColumnInfo(name = "jan")
    private float janAmount;

    @ColumnInfo(name = "feb")
    private float febAmount;

    @ColumnInfo(name = "mar")
    private float marAmount;

    @ColumnInfo(name = "apr")
    private float aprAmount;

    @ColumnInfo(name = "may")
    private float mayAmount;

    @ColumnInfo(name = "jun")
    private float junAmount;

    @ColumnInfo(name = "jul")
    private float julAmount;

    @ColumnInfo(name = "aug")
    private float augAmount;

    @ColumnInfo(name = "sep")
    private float sepAmount;

    @ColumnInfo(name = "oct")
    private float octAmount;

    @ColumnInfo(name = "nov")
    private float novAmount;

    @ColumnInfo(name = "dec")
    private float decAmount;

    @ColumnInfo(name = "total")
    private float totalAmount;

    public Income(int mId, float janAmount, float febAmount, float marAmount, float aprAmount, float mayAmount, float junAmount, float julAmount, float augAmount, float sepAmount, float octAmount, float novAmount, float decAmount, float totalAmount)
    {
        this.mId = mId;
        this.janAmount = janAmount;
        this.febAmount = febAmount;
        this.marAmount = marAmount;
        this.aprAmount = aprAmount;
        this.mayAmount = mayAmount;
        this.junAmount = junAmount;
        this.julAmount = julAmount;
        this.augAmount = augAmount;
        this.sepAmount = sepAmount;
        this.octAmount = octAmount;
        this.novAmount = novAmount;
        this.decAmount = decAmount;
        this.totalAmount = totalAmount;
    }


    public float getAprAmount() {
        return aprAmount;
    }

    public float getAugAmount() {
        return augAmount;
    }

    public float getDecAmount() {
        return decAmount;
    }

    public float getFebAmount() {
        return febAmount;
    }

    public float getJanAmount() {
        return janAmount;
    }

    public float getJulAmount() {
        return julAmount;
    }

    public float getJunAmount() {
        return junAmount;
    }

    public float getMarAmount() {
        return marAmount;
    }

    public float getMayAmount() {
        return mayAmount;
    }

    public float getNovAmount() {
        return novAmount;
    }

    public float getOctAmount() {
        return octAmount;
    }

    public float getSepAmount() {
        return sepAmount;
    }

    public float getTotalAmount() {
        return totalAmount;
    }

    public int getmId() {
        return mId;
    }
}
