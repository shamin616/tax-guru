package com.retinaapps.taxguru.Database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Created by apple on 27/01/18.
 */
@Dao
public interface IncomeDao {

    @Query("SELECT * FROM income WHERE id = :id ")
    Income findById(int id);

    @Query("SELECT * FROM income")
    List<Income> getAll();

    @Insert
    void insert(Income income);

    @Delete
    void delete(Income income);

    @Update
    void update(Income income);
}
