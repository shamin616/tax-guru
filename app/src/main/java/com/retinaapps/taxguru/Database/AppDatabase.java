package com.retinaapps.taxguru.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

/**
 * Created by apple on 27/01/18.
 */
@Database(entities = {Savings.class,Income.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract SavingsDao savingsDao();
    public abstract IncomeDao incomeDao();
}
