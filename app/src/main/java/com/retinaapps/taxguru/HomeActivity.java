package com.retinaapps.taxguru;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity implements HomeActivityFragment.SetTaxAmount {

    //CardView BottomTaxSheet;
    TextView taxTextView;
    TextView taxAmountTextView;
    TextView taxSavingsTextView;
    TextView taxPercentTextView;
    FloatingActionButton addDataFab;
    CollapsingToolbarLayout collapsingToolbarLayout;
    AppBarLayout appBarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar);

        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar_layout);

        addDataFab = findViewById(R.id.add_data_fab);

        // BottomTaxSheet = findViewById(R.id.tax_bottom_sheet);

       // BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(BottomTaxSheet);
       // bottomSheetBehavior.setHideable(false);

//        taxTextView = BottomTaxSheet.findViewById(R.id.tax_amount_text);
//        taxAmountTextView = BottomTaxSheet.findViewById(R.id.tax_total_amount_text);
//        taxSavingsTextView = BottomTaxSheet.findViewById(R.id.tax_savings_amount_text);
//        taxPercentTextView = BottomTaxSheet.findViewById(R.id.tax_percent_text);
        addDataFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }


    @Override
    public void setAmount(float income,float savings) {
        int incomeText = Math.round(income);
        int savingsText = Math.round(savings);
        int taxableAmount = incomeText - savingsText;
        if (income == 0)
        {
            setNonCollapsibleToolbar();
        }
        else
        {
            setCollapsibleToolbar();
        }
        ((TextView) collapsingToolbarLayout.findViewById(R.id.tax_amount_text_home)).setText("₹"+Integer.toString(taxableAmount * getTaxPercent(taxableAmount)/100));
        ((TextView) collapsingToolbarLayout.findViewById(R.id.tax_total_amount_text_home)).setText("₹"+Integer.toString(incomeText));
        ((TextView) collapsingToolbarLayout.findViewById(R.id.tax_savings_amount_text_home)).setText("₹"+Integer.toString(savingsText));
        ((TextView) collapsingToolbarLayout.findViewById(R.id.tax_percent_text_home)).setText(Integer.toString(getTaxPercent(taxableAmount))+"%");
//        taxTextView.setText(Float.toString(taxableAmount * getTaxPercent(taxableAmount)/100));
//        taxAmountTextView.setText(Float.toString(income));
//        taxSavingsTextView.setText(Float.toString(savings));
//        taxPercentTextView.setText(Integer.toString(getTaxPercent(taxableAmount))+"%");
    }

    public void setNonCollapsibleToolbar()
    {
        CoordinatorLayout.LayoutParams params =(CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
        params.height = 3*70;
        appBarLayout.setLayoutParams(params);
        appBarLayout.setExpanded(false);
    }
    public void setCollapsibleToolbar()
    {
        CoordinatorLayout.LayoutParams params =(CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
        params.height = CoordinatorLayout.LayoutParams.WRAP_CONTENT;
        appBarLayout.setLayoutParams(params);
        appBarLayout.setExpanded(true);
    }
    public int getTaxPercent(float amount)
    {
        if(amount <= 250000)
        {
            return 0;
        }
        else if(amount <= 500000)
        {
            return 5;
        }
        else if(amount <= 1000000)
        {
            return 20;
        }
        else
        {
            return 30;
        }
    }
}
