package com.retinaapps.taxguru;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.retinaapps.taxguru.Data.SavingsTypes;
import com.retinaapps.taxguru.Database.AppDatabase;
import com.retinaapps.taxguru.Database.Income;
import com.retinaapps.taxguru.Database.Savings;

import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class HomeActivityFragment extends Fragment implements HomeRecyclerViewAdapter.OnItemClickListener {

    int REFRESH_TAX = 1;
    RecyclerView homeRecyclerView;
    ArrayList<HomeCards> homeCards;
    HomeRecyclerViewAdapter homeRecyclerViewAdapter;
    public static final String SAVINGS_ID = "savings_id";
    public boolean isIncomeAdded = false;

    static final int TYPE_ADD_INCOME = 0;
    static final int TYPE_ADD_SAVINGS = 1;
    static final int TYPE_SAVINGS = 2;

    public HomeActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        homeRecyclerView = rootView.findViewById(R.id.home_recycler_view);

        homeRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        homeCards = new ArrayList<>();
        setHomeCards();
        homeRecyclerViewAdapter = new HomeRecyclerViewAdapter(homeCards, this, getContext());
        homeRecyclerView.setAdapter(homeRecyclerViewAdapter);

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REFRESH_TAX) {
            refreshTaxAmount();
        }
    }

    public void setHomeCards() {
        homeCards.clear();
        SetHomeCards setHomeCards = new SetHomeCards();
        setHomeCards.execute();
    }

    public void setIncomeCard() {
        //TODO : Check if income is in database and show proper card
        if (isIncomeAdded) {
            homeCards.add(new HomeCards(TYPE_ADD_SAVINGS, "Add your savings", "Add your savings", R.drawable.ic_savings));
        }
    }

    public void refreshTaxAmount() {
        setHomeCards();
    }

    @Override
    public void onClick(HomeCards homeCard) {
        Intent intent;
        switch (homeCard.type) {
            case TYPE_ADD_INCOME:
                intent = new Intent(getContext(), AddIncomeActivity.class);
                startActivityForResult(intent, REFRESH_TAX);
                break;
            case TYPE_ADD_SAVINGS:
                intent = new Intent(getContext(), SavingsChooseActivity.class);
                startActivityForResult(intent, REFRESH_TAX);
                break;
            case TYPE_SAVINGS:
                intent = new Intent(getContext(), SavingsActivity.class);
                intent.putExtra(SAVINGS_ID, homeCard.getSavings_id());
                startActivityForResult(intent, REFRESH_TAX);
                break;
            default:
                Log.e("Clicked", homeCard.getHeading());

        }
    }

    public class SetHomeCards extends AsyncTask<Void, Void, Void> {
        float totalIncome = 0;
        float totalSavings = 0;

        @Override
        protected Void doInBackground(Void... params) {
            AppDatabase db = Room.databaseBuilder(getContext(),
                    AppDatabase.class, "com.retinaapps.taxguru").build();
            List<Income> income = db.incomeDao().getAll();
            if (income.size() != 0) {
                if (income.get(0).getTotalAmount() != 0)
                    isIncomeAdded = true;
                totalIncome = income.get(0).getTotalAmount();
                homeCards.add(new HomeCards(TYPE_ADD_INCOME, "₹" + Float.toString(income.get(0).getTotalAmount()), "Income", R.drawable.ic_income));
            } else {
                homeCards.add(new HomeCards(TYPE_ADD_INCOME, "Add your income", "Add your income", R.drawable.ic_income));
            }
            List<Savings> savings = db.savingsDao().getAll();
            SavingsTypes savingsTypes = new SavingsTypes();
            totalSavings = 0;
            for (Savings saving : savings) {
                totalSavings += saving.getAmount();
                homeCards.add(new HomeCards(TYPE_SAVINGS, "₹" + Float.toString(saving.getAmount()), savingsTypes.getSavingsType(saving.getType()).getmType(), saving.getId(), savingsTypes.getSavingsType(saving.getType()).getmIcon()));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            homeRecyclerViewAdapter.notifyDataSetChanged();
            SetTaxAmount setTaxAmount = (SetTaxAmount) getActivity();
            setTaxAmount.setAmount(totalIncome, totalSavings);
            setIncomeCard();
        }

    }

    public interface SetTaxAmount {
        void setAmount(float Income, float Savings);
    }
}
