package com.retinaapps.taxguru;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;

/**
 * Created by apple on 28/01/18.
 */

public class HomeRecyclerViewAdapter  extends RecyclerView.Adapter<HomeRecyclerViewAdapter.ViewHolder> {

    private ArrayList<HomeCards> mHomeCards;
    private OnItemClickListener mListener;
    private Context context;

    HomeRecyclerViewAdapter(ArrayList<HomeCards> items, OnItemClickListener listener,Context context) {
        mHomeCards = items;
        mListener = listener;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_cards, parent, false);
        return new HomeRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mHomeCards.get(position);
        holder.mTextMain.setText(mHomeCards.get(position).getHeading());
        holder.mTextDetail.setText(mHomeCards.get(position).getDescription());
        holder.mIcon.setImageResource(mHomeCards.get(position).getIcon());
        holder.mIcon.setColorFilter(ContextCompat.getColor(context, R.color.color_icon_pink), android.graphics.PorterDuff.Mode.SRC_IN);


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onClick(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mHomeCards.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTextMain;
        public final TextView mTextDetail;
        public final ImageView mIcon;
        public HomeCards mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTextMain = (TextView) view.findViewById(R.id.home_card_text_main);
            mTextDetail = (TextView) view.findViewById(R.id.home_card_text_detail);
            mIcon =  view.findViewById(R.id.home_card_icon);
        }
    }

    public interface OnItemClickListener
    {
        void onClick(HomeCards homeCard);
    }
}
