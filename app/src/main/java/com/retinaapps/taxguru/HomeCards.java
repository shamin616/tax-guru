package com.retinaapps.taxguru;

/**
 * Created by apple on 28/01/18.
 */

public class HomeCards {
    int type;
    int savings_id;
    String heading;
    String description;
    private int icon;

    public HomeCards(int type,String heading,String description,int icon)
    {
        this.type = type;
        this.heading = heading;
        this.description = description;
        this.icon = icon;
    }
    public HomeCards(int type,String heading,String description,int savings_id,int icon)
    {
        this.type = type;
        this.heading = heading;
        this.description = description;
        this.savings_id = savings_id;
        this.icon = icon;
    }

    public String getHeading() {
        return heading;
    }

    public String getDescription() {
        return description;
    }

    public int getSavings_id() {
        return savings_id;
    }

    public int getIcon() {
        return icon;
    }
}
