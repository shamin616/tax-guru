package com.retinaapps.taxguru.Data;

import com.retinaapps.taxguru.R;

import java.util.ArrayList;

/**
 * Created by apple on 27/01/18.
 */

public class SavingsTypes {
    private ArrayList<SavingsType> savingsTypes;
    public SavingsTypes()
    {
        savingsTypes = new ArrayList<>();
        savingsTypes.add(new SavingsType(1,"PPF (Public Provident Fund)", R.drawable.ic_ppf));
        savingsTypes.add(new SavingsType(2,"EPF (Employees’ Provident Fund)",R.drawable.ic_epf));
        savingsTypes.add(new SavingsType(3,"5 years Bank or Post office Tax saving Deposits",R.drawable.ic_po_savings));
        savingsTypes.add(new SavingsType(4,"National Savings Certificates (NSC)",R.drawable.ic_nsc));
    }
    public SavingsType getSavingsType(int id)
    {
        return savingsTypes.get(id-1);
    }

    public ArrayList<SavingsType> getSavingsTypes() {
        return savingsTypes;
    }

    public class SavingsType
    {
        private int mId;
        private String mType;
        private int mIcon;
        SavingsType(int id,String type,int icon)
        {
            mId = id;
            mType = type;
            mIcon = icon;
        }

        public String getmType() {
            return mType;
        }

        public int getmId() {
            return mId;
        }

        public int getmIcon() {
            return mIcon;
        }

    }
}
