package com.retinaapps.taxguru;

import android.arch.persistence.room.Room;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.retinaapps.taxguru.Database.AppDatabase;
import com.retinaapps.taxguru.Database.Income;

import java.util.List;

public class AddIncomeMonthly extends AppCompatActivity {

    TextInputEditText janEditText;
    TextInputEditText febEditText;
    TextInputEditText marEditText;
    TextInputEditText aprEditText;
    TextInputEditText mayEditText;
    TextInputEditText junEditText;
    TextInputEditText julEditText;
    TextInputEditText augEditText;
    TextInputEditText sepEditText;
    TextInputEditText octEditText;
    TextInputEditText novEditText;
    TextInputEditText decEditText;
    float totalIncome = -1f;
    Button SubmitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_income_monthly);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        janEditText = findViewById(R.id.janAmount);
        febEditText = findViewById(R.id.febAmount);
        marEditText = findViewById(R.id.marAmount);
        aprEditText = findViewById(R.id.aprAmount);
        mayEditText = findViewById(R.id.mayAmount);
        junEditText = findViewById(R.id.junAmount);
        julEditText = findViewById(R.id.julAmount);
        augEditText = findViewById(R.id.augAmount);
        sepEditText = findViewById(R.id.sepAmount);
        octEditText = findViewById(R.id.octAmount);
        novEditText = findViewById(R.id.novAmount);
        decEditText = findViewById(R.id.decAmount);

        GetIncome getIncome = new GetIncome();
        getIncome.execute();

        SubmitButton = findViewById(R.id.monthly_income_save_button);
        SubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Float janAmount =  Float.valueOf(janEditText.getText().toString());
                Float febAmount =  Float.valueOf(febEditText.getText().toString());
                Float marAmount =  Float.valueOf(marEditText.getText().toString());
                Float aprAmount =  Float.valueOf(aprEditText.getText().toString());
                Float mayAmount =  Float.valueOf(mayEditText.getText().toString());
                Float junAmount =  Float.valueOf(junEditText.getText().toString());
                Float julAmount =  Float.valueOf(julEditText.getText().toString());
                Float augAmount =  Float.valueOf(augEditText.getText().toString());
                Float sepAmount =  Float.valueOf(sepEditText.getText().toString());
                Float octAmount =  Float.valueOf(octEditText.getText().toString());
                Float novAmount =  Float.valueOf(novEditText.getText().toString());
                Float decAmount =  Float.valueOf(decEditText.getText().toString());
                AddIncome addIncome = new AddIncome();
                addIncome.execute(janAmount,febAmount,marAmount,aprAmount,mayAmount,junAmount,julAmount,augAmount,sepAmount,octAmount,novAmount,decAmount);
            }
        });
    }
    private class AddIncome extends AsyncTask<Float, Void, Void> {
        @Override
        protected Void doInBackground(Float... params) {
            AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                    AppDatabase.class, "com.retinaapps.taxguru").build();
            float total = params[0]+params[1]+params[2]+params[3]+params[4]+params[5]+params[6]+params[7]+params[8]+params[9]+params[10]+params[11];
            Income income = new Income(1,params[0],params[1],params[2],params[3],params[4],params[5],params[6],params[7],params[8],params[9],params[10],params[11],total);
            //TODO: Check if income exists
            if(totalIncome == -1f)
            {
                db.incomeDao().insert(income);
            }
            else
            {
                db.incomeDao().update(income);
            }
            return null;
        }
    }
    private class GetIncome extends AsyncTask<Void, Void, Income> {
        @Override
        protected Income doInBackground(Void... params) {
            AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                    AppDatabase.class, "com.retinaapps.taxguru").build();

            return db.incomeDao().findById(1);
        }

        @Override
        protected void onPostExecute(Income income) {
            super.onPostExecute(income);
            if(income!=null)
            {
                janEditText.setText(Float.toString(income.getJanAmount()));
                febEditText.setText(Float.toString(income.getFebAmount()));
                marEditText.setText(Float.toString(income.getMarAmount()));
                aprEditText.setText(Float.toString(income.getAprAmount()));
                mayEditText.setText(Float.toString(income.getMayAmount()));
                junEditText.setText(Float.toString(income.getJunAmount()));
                julEditText.setText(Float.toString(income.getJulAmount()));
                augEditText.setText(Float.toString(income.getAugAmount()));
                sepEditText.setText(Float.toString(income.getSepAmount()));
                octEditText.setText(Float.toString(income.getOctAmount()));
                novEditText.setText(Float.toString(income.getNovAmount()));
                decEditText.setText(Float.toString(income.getDecAmount()));
                totalIncome = income.getTotalAmount();
            }
        }
    }

}
